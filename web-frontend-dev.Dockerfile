# This a dev image for testing your plugin when installed into the Baserow web-frontend image

# TODO before merge switch back to proper dockerhub image
FROM registry.gitlab.com/bramw/baserow/ci/web-frontend:ci-latest-432-update-plugin-boilterplate-and-docs-to-match-new-docker-usage as base
FROM registry.gitlab.com/bramw/baserow/ci/web-frontend:ci-latest-432-update-plugin-boilterplate-and-docs-to-match-new-docker-usage

USER root

ARG PLUGIN_BUILD_UID
ENV PLUGIN_BUILD_UID=${PLUGIN_BUILD_UID:-9999}
ARG PLUGIN_BUILD_GID
ENV PLUGIN_BUILD_GID=${PLUGIN_BUILD_GID:-9999}

# If we aren't building as the same user that owns all the files in the base
# image/installed plugins we need to chown everything first.
COPY --from=base --chown=$PLUGIN_BUILD_UID:$PLUGIN_BUILD_GID /baserow /baserow

COPY --chown=$PLUGIN_BUILD_UID:$PLUGIN_BUILD_GID ./plugins/baserow_example_formula_plugin/ $BASEROW_PLUGIN_DIR/baserow_example_formula_plugin/
RUN /baserow/plugins/install_plugin.sh --folder $BASEROW_PLUGIN_DIR/baserow_example_formula_plugin --dev

USER $PLUGIN_BUILD_UID:$PLUGIN_BUILD_GID
CMD ["nuxt-dev"]
