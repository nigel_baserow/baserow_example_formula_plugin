# TODO before merge switch back to proper dockerhub image
FROM registry.gitlab.com/bramw/baserow/ci/backend:ci-latest-432-update-plugin-boilterplate-and-docs-to-match-new-docker-usage

USER root

COPY ./plugins/baserow_example_formula_plugin/ $BASEROW_PLUGIN_DIR/baserow_example_formula_plugin/
RUN /baserow/plugins/install_plugin.sh --folder $BASEROW_PLUGIN_DIR/baserow_example_formula_plugin

USER $UID:$GID
