from baserow.core.registries import plugin_registry
from django.apps import AppConfig


class PluginNameConfig(AppConfig):
    name = "baserow_example_formula_plugin"

    def ready(self):
        from .plugins import PluginNamePlugin

        plugin_registry.register(PluginNamePlugin())
        from baserow.contrib.database.formula.registries import (
            formula_function_registry,
        )

        from .example_formula_function import BaserowTimezone

        formula_function_registry.register(BaserowTimezone())
