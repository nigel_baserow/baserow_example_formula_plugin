
import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = []
    initial = True

    operations = [
        migrations.RunSQL(
            (
                """
CREATE OR REPLACE FUNCTION safe_timezone( date TIMESTAMP, tz TEXT ) RETURNS TIMESTAMPTZ
as $$
DECLARE
BEGIN
    RETURN date AT TIME ZONE tz;
EXCEPTION WHEN invalid_parameter_value THEN
    RETURN null;
END;
$$ language plpgsql STABLE;
CREATE OR REPLACE FUNCTION safe_timezone( date TIMESTAMP WITH TIME ZONE, tz TEXT ) RETURNS 
TIMESTAMPTZ
as $$
DECLARE
BEGIN
    RETURN date AT TIME ZONE tz;
EXCEPTION WHEN invalid_parameter_value THEN
    RETURN null;
END;
$$ language plpgsql STABLE;
    """
            ),
            ("DROP FUNCTION IF EXISTS safe_timezone(date, tz);"),
        ),
    ]
