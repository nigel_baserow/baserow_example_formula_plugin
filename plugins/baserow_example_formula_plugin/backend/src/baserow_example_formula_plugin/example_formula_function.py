from baserow.contrib.database.formula.ast.function import (
    TwoArgumentBaserowFunction,
)
from baserow.contrib.database.formula.ast.tree import (
    BaserowFunctionCall,
    BaserowExpression,
)
from baserow.contrib.database.formula.types.formula_type import (
    BaserowFormulaType,
    BaserowFormulaValidType,
    UnTyped,
)
from baserow.contrib.database.formula.types.formula_types import (
    BaserowFormulaDateType,
    BaserowFormulaTextType,
)
from django.db.models import (
    Expression,
    fields,
    Func,
)


class BaserowTimezone(TwoArgumentBaserowFunction):
    type = "timezone"

    arg1_type = [BaserowFormulaDateType]
    arg2_type = [BaserowFormulaTextType]

    def type_function(
        self,
        func_call: BaserowFunctionCall[UnTyped],
        arg1: BaserowExpression[BaserowFormulaValidType],
        arg2: BaserowExpression[BaserowFormulaValidType],
    ) -> BaserowExpression[BaserowFormulaType]:
        return func_call.with_valid_type(arg1.expression_type)

    def to_django_expression(self, arg1: Expression, arg2: Expression) -> Expression:
        return Func(
            arg1, arg2, function="safe_timezone", output_field=fields.DateTimeField()
        )
