#!/bin/bash
# Bash strict mode: http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail

# This file is automatically run by Baserow when the plugin is uninstalled.

# Baserow will automatically `pip uninstall` the plugin for you so no need to do that
# in here.

./baserow migrate baserow_example_formula_plugin zero
