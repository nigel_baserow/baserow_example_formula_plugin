import { PluginNamePlugin } from '@baserow-example-formula-plugin/plugins'
import { BaserowTimezone } from '@baserow-example-formula-plugin/exampleFormulaFunction'

export default (context) => {
  const { app } = context
  app.$registry.register('plugin', new PluginNamePlugin(context))
  app.$registry.register('formula_function', new BaserowTimezone(context))
}
