import { BaserowFunctionDefinition } from '@baserow/modules/database/formula/functions'
export class BaserowTimezone extends BaserowFunctionDefinition {
  static getType() {
    return 'timezone'
  }

  getDescription() {
    return 'Converts a date to a different timezone'
  }

  getSyntaxUsage() {
    return ['timezone(date, timezone)']
  }

  getExamples() {
    return ["timezone(todate('20210101', 'YYYYMMDD'), 'Europe/London')"]
  }

  getFormulaType() {
    return 'date'
  }
}
